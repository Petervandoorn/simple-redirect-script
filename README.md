Super simple script that allows to parse a URL as parameter and redirect to the given URL. The point of this to trigger an analytics event. 

So: 
1. You send a marketing mailing with a embedded URL in the text 
2. The URL in the email is https://LocationOfThisScript.com/redirect.html?url=ThePageYouWantToRedirectTo.com
3. If a recipient of the email clicks on the link then the script ensures that an analytics event is triggered to Matomo, and then redirects the user to the actual page
4. The neat thing is just before triggering the analytics event it updates the page title so that Matomo only registers the page you redirect to 
